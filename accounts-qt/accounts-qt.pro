######################################################################
#                                                                    #
######################################################################
include(../../examples.pri)

TEMPLATE = app
TARGET = accounts-qt-example
CONFIG += link_pkgconfig
PKGCONFIG += accounts-qt QtXml
DEPENDPATH += .
INCLUDEPATH += .

# Input
HEADERS += accountsexample.h model.h
SOURCES += accountsexample.cpp model.cpp
FORMS   += accountsexample.ui
INSTALLS += target
