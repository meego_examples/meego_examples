/* The code examples copyrighted by Nokia Corporation that are included to
 * this material are licensed to you under following MIT-style License:
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */
#ifndef ACCOUNTSEXAMPLE_H
#define ACCOUNTSEXAMPLE_H

#include <accounts-qt/account.h>
#include <accounts-qt/manager.h>

using namespace Accounts;

#include "ui_accountsexample.h"
#include "model.h"


/*!
 * The AccountsExample class implements the slots
 */
class AccountsExample : public QMainWindow, Ui::MainWindow
{
    Q_OBJECT

public:
    /*!
     *  Constructor sets a Model
     *  sets up a tmp directory for the services database
     */
    AccountsExample();
    /*!
     * The Destructor.
     */
    ~AccountsExample();

    /*!
     * slots for the implementation
     */
private slots:
    void on_accountListView_clicked(const QModelIndex &index);
    void on_addButton_clicked();
    void on_removeButton_clicked();
    void on_enableDisableButton_clicked();

    void on_exitButton_clicked();

private:
    Accounts::Manager *accountsManager;
    Model* testAccountModel;
    ServiceList mgrServiceList;
};
#endif
