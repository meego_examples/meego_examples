/* The code examples copyrighted by Nokia Corporation that are included to
 * this material are licensed to you under following MIT-style License:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <QApplication>
#include <QDebug>
#include <QTreeView>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QDir>
//accounts
#include <accounts-qt/service.h>
#include <accounts-qt/provider.h>

#include "accountsexample.h"
#include "model.h"


static void clearDb()
{
    QDir dbroot(getenv("ACCOUNTS"));
    dbroot.remove(QString("accounts.db"));
}

AccountsExample::AccountsExample()
{
    setupUi(this);
    setenv("ACCOUNTS", "/tmp", true);
    accountsManager = new Accounts::Manager();
    testAccountModel = new Model(accountsManager);

    accountListView->setModel(testAccountModel);
    accountListView->resizeColumnsToContents();

	// Get list of services available, these are service plugins
	// installed and available in system environment.
    mgrServiceList = accountsManager->serviceList();

	// Add the service names to combo box.
    for(int i = 0; i < mgrServiceList.count(); i++)
        comboBox->addItem(mgrServiceList.at(i)->displayName());
}

AccountsExample::~AccountsExample()
{
    clearDb();
    //    clearenv();
}

// User clicked list item, set enable / disable account button text accordingly
void AccountsExample::on_accountListView_clicked(const QModelIndex &index)
{
    Accounts::Account *selectedAccount = testAccountModel->account(index);
    if (selectedAccount->enabled()) {
        enableDisableButton->setText("Disable");
    }
    else {
        enableDisableButton->setText("Enable");
    }
}

//Add button clicked, add account
void AccountsExample::on_addButton_clicked()
{
	// Check if selected service name is vaid and add to account db.
    if(!comboBox->currentText().isEmpty())
    {
		// Get the selected service index in combo box.
        int i = comboBox->currentIndex();

		// Create account for the selected service in account db.
        Account *account = accountsManager->createAccount(mgrServiceList.at(i)->provider());

		// Check if any error has occurred.
        Error err = accountsManager->lastError();
        if(err.type() == Error::NoError)
        {
            qDebug()<<"Account created successfully";
        }
        else
        {
            qDebug()<<"Account is not created,some error occured.";

        }

		// Enable created account.
        account->setEnabled(true);
	
		// Select the created service.
        account->selectService(mgrServiceList.at(i));

		// Compose the display name of the service.
        QString acctName(mgrServiceList.at(i)->displayName()+" ( Test Account )");
        account->setDisplayName(acctName);

		// Sync the account changes to persistent db.
        account->sync();

		// Resize table view to adopt to content displayed.
        accountListView->resizeColumnsToContents();
    }
}

//Remove button clicked, remove all selected accounts
void AccountsExample::on_removeButton_clicked()
{
    QItemSelectionModel *selectionModel = accountListView->selectionModel();
    QModelIndexList indexes = selectionModel->selectedIndexes();
    QModelIndex index;
    Accounts::Account *account;
    foreach(index, indexes) {
        if (index.column() == 0) {
            account = testAccountModel->account(index);
            account->remove();
            account->sync();
        }
    }
}

// Enable/Disable button clicked, set account status accordingly
void AccountsExample::on_enableDisableButton_clicked()
{
    QItemSelectionModel *selectionModel = accountListView->selectionModel();
    QModelIndexList indexes = selectionModel->selectedIndexes();
    QModelIndex index;
    Accounts::Account *account;
    foreach(index, indexes) {
        if (index.column() == 0) {
            account = testAccountModel->account(index);
            if (account->enabled()) {
                account->setEnabled(false);
                enableDisableButton->setText("Enable");
            }
            else {
                account->setEnabled(true);
                enableDisableButton->setText("Disable");
            }
            account->sync();

			// Refresh the table view to display modifications to model.
            accountListView->clearSelection();
        }
    }
}



int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    AccountsExample accountsExample ;
    accountsExample.show();
    return app.exec();
}






void AccountsExample::on_exitButton_clicked()
{
    clearDb();
    this->close();
}
