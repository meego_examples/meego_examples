/* The code examples copyrighted by Nokia Corporation that are included to
 * this material are licensed to you under following MIT-style License:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "model.h"

//accounts
#include <accounts-qt/account.h>
#include <accounts-qt/service.h>
#include <accounts-qt/manager.h>

using namespace Accounts;

//Qt
#include <QDebug>
#include <QPointer>


Model::Model(Accounts::Manager *parent)
    : QAbstractListModel(parent)
{

    m_headerData.insert(CT_AccountId, "accountId");
    m_headerData.insert(CT_DisplayName, "displayName");
    m_headerData.insert(CT_ProviderName, "providerName");
    m_headerData.insert(CT_No_ofServices, "Services");
    m_headerData.insert(CT_Enabled, "enabled");
    m_headerData.insert(CT_Count, "count");
    QObject::connect(parent, SIGNAL(accountCreated(Accounts::AccountId)),
                     this, SLOT(accountCreated(Accounts::AccountId)));
    QObject::connect(parent, SIGNAL(accountRemoved(Accounts::AccountId)),
                     this, SLOT(accountRemoved(Accounts::AccountId)));
    accounts = parent->accountList();
}

Model::~Model()
{
}

void Model::accountCreated(Accounts::AccountId id)
{
    Accounts::AccountIdList new_accounts = accountManager()->accountList();

    /* find the position of the new account in the list: QAbstractItemModel
     * APIs need it */
    int index = new_accounts.indexOf(id);
    if (index < 0) {
        // Account id not present in the new list
        return;
    }

    QModelIndex parent;
    beginInsertRows(parent, index, index);
    accounts = new_accounts;
    endInsertRows();
}

void Model::accountRemoved(Accounts::AccountId id)
{
    Accounts::AccountIdList new_accounts = accountManager()->accountList();

    /* find the position of the deleted account in the list: QAbstractItemModel
     * APIs need it */
    int index = accounts.indexOf(id);
    if (index < 0) {
        // Account id not present in the new list
        return;
    }

    QModelIndex parent;
    beginRemoveRows(parent, index, index);
    accounts = new_accounts;
    endRemoveRows();
}

bool Model::isEmpty() const
{
    return (0 >= rowCount());
}

Accounts::Account *Model::account(const QModelIndex &index) const
{
    return accountManager()->account(index.internalId());
}

Accounts::Manager *Model::accountManager() const
{
    return qobject_cast<Accounts::Manager *>(QObject::parent());
}

QModelIndex Model::index(int row, int column, const QModelIndex &parent) const
{
    if (row < 0 || column < 0 || !hasIndex(row, column, parent))
        return QModelIndex();

    return createIndex(row, column, accounts.at(row));
}

int Model::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return accounts.length();
}

int Model::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return CT_Count;
}

QVariant Model::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    Accounts::AccountId accountId = index.internalId();
    Accounts::Account *account = accountManager()->account(accountId);

    if (role == Qt::DisplayRole) {
        switch (index.column()) {
        case CT_AccountId:
            return accountId;
        case CT_DisplayName:
            return account->displayName();
        case CT_ProviderName:
            return account->providerName();
        case CT_No_ofServices:
            return account->services().count();
        case CT_Enabled:
            return account->enabled();
        }
        return account->displayName();
    }
    return QVariant();
}

QVariant Model::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation != Qt::Horizontal) {
        return QVariant();
    }
    ColumnTypes column = static_cast<ColumnTypes>(section);

    if (role == Qt::DisplayRole) {
        if (section < m_headerData.size()) {
            return m_headerData.value(column);
        }
    }
    return QVariant();
}
