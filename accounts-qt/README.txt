APPLICATION
        This is an example application showing usage and purpose of Qt Accounts API.

DESCRIPTION
        This application demonstrates the usage of Qt Accounts APIs.
        It is a GUI application that can be used to view available services, select, enable or disable
	 selected services.

BUILDING
        The application is using Qt build system, so use usual qmake/make:
        $ qmake
        $ make

        NOTE: You need "libaccounts-qt-dev" package to build this application.
	      You also need account service plugins to use this application completely. For example, the 
		plugins this application is tested are "account-plugin-google-talk account-plugin-gmail
		and account-plugin-att-sync".
		

RUNNING
        You can run the application by typing meego-run ./accounts-qt-example from 
	qtm-tests/examples/accounts-qt/accounts-qt directory.
	
	Steps:
	    1. After running the application.
	    2. You can select a service from drop down list on top of application and click "Add" button to
		add them to accounts ( if necessary plugins are installed and available ). Then selected service
		should be visible in table view below and displayed account id must be unique.
	    3. You can also select a service from list and click "Enable / Disable" button based on context to enable or
		disable the selected service. Then you should see proper state in the "Enable" column.
	    4. Select a service from the table view and click "Remove" button to remove the service from account. Then the
		selected service must be removed from table view and account.
	    5. Click "Exit" button to quit from application. This will also commit all the service removal changes to persistent DB.

README (END)

