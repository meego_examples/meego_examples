/* The code examples copyrighted by Nokia Corporation that are included to
 * this material are licensed to you under following MIT-style License:
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef ACCOUNTMODEL_H
#define ACCOUNTMODEL_H

//Accounts
#include <accounts-qt/account.h>
#include <accounts-qt/manager.h>

using namespace Accounts;

//Qt
#include <QAbstractTableModel>
#include <QMap>
#include <QVariant>


/*!
 * The Accountmodel gets a list of accounts from Accounts::Manager
 */
class Model : public QAbstractListModel
{
    Q_OBJECT

public:
    /*!
     * This enumartor holds the names for the used/provided columns
     */
    enum ColumnTypes { CT_AccountId,
                       CT_DisplayName,
                       CT_ProviderName,
                       CT_No_ofServices,
                       CT_Enabled,
                       CT_Count };

    /*!
     *  Constructor creates an Model
     * \param parent The parent Object.
     */
    Model(Accounts::Manager *parent);

    /*!
     * The Destructor.
     */
    ~Model();

    /*!
     *  Derived from QAbstractListModel - delivers the number of rows in the model.
     * \return The number of valid Accounts given by the AccountManager or 0 if index is a valid
     * QModelIndex
     * \param index Will lead to returning 0 if it is a valid QModelIndex. Since we have a simple
     *  Listmodel and no treemodel there should be no need to set this.
     */
    int rowCount(const QModelIndex &index = QModelIndex()) const;

    /*!
     *  Derived from QAbstractListModel - returns the number of columns in the model.
     * \return The number of columns in the model or 0 if <i> index </i> is a valid QModelIndex
     * \param index Will lead to returning 0 if it is a valid QModelIndex. Since we have a simple
     *  Listmodel and no treemodel there should be no need to set this.
     */
    int columnCount(const QModelIndex &index) const;

    /*!
     *  Provides a getter for the model-data depending on the index and the role provided as
     *  Parameter.
     * \return ModelData depending on given <i>index</i> and <i>role</i> capsuled in a QVariant.
     * \param index A QModelindex that is related to the requested data
     * \param role The role the data is requested for \ref Qt::ItemDataRole.
     */
    QVariant data(const QModelIndex &index, int role) const;

    /*!
     *  Derived from QAbstractTableModel - Provides a way to get a QModelIndex for a certain
     *  <i>row</i>, <i>column</i> and <i>parent</i> combination.
     * \return A QModelIndex related to the given combination or a QModelIndex() if given row is
     *   invalid.
     * \param row The row of the requested index.
     * \param column The column for the requested index;
     * \param parent The QModelIndex of the parent of the requested index' parent
     */
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;

    /*!
     *  Can be used to clerify wether the model is empty or not.
     * \return True if there is no date in the model or false if there is something
     */
    bool isEmpty() const;
    /*!
     *  Get the account for a certain modelIndex
     * \return Returns a pointer to an account if it was found or 0 if not.
     */
  Account* account(const QModelIndex &index) const;

  Manager* accountManager() const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;


private slots:
    void accountCreated(Accounts::AccountId id);
    void accountRemoved(Accounts::AccountId id);

private:
  AccountIdList accounts;
    QMap<ColumnTypes, QVariant> m_headerData;

};

#endif //ACCOUNTMODEL_H
