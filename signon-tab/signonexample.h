/* The code examples copyrighted by Nokia Corporation that are included to
 * this material are licensed to you under following MIT-style License:
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef SIGNONCLIENT_H
#define SIGNONCLIENT_H

#include "SignOn/AuthService"
#include "SignOn/AuthSession"
#include "SignOn/Identity"

#include "ui_signonexample-tab.h"


class SignonExample : public QMainWindow, Ui::MainWindow {

    Q_OBJECT

public:
    SignonExample();
    ~SignonExample();

private slots:
    void methodsAvailable(const QStringList &mechs);
    void mechanismsAvailable(const QString &method, const QStringList &mechs);
    void identities(const QList<SignOn::IdentityInfo>& identityList);

    void error(const SignOn::Error &err);

    void sessionResponse(const SignOn::SessionData &sessionData);
    void credentialsStored(const quint32 id);

    void userInfoAvailable(const SignOn::IdentityInfo& info);
    void identityMethodsAvailable(const QStringList &methods);
    void identityRemoved();

    // Auto-connecting slots
    void on_removeIdentityButton_clicked();
    void on_methodsComboBox_activated(const QString & text);
    void on_storeButton_clicked();
    void on_challengeButton_clicked();
    void on_identityListWidget_itemSelectionChanged();

    void on_exitButton_clicked();

private:
    SignOn::AuthService* authService;
};

#endif
