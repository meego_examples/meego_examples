APPLICATION
       This is an example application showing usage and purpose of Qt Signon API.

DESCRIPTION
        This application demonstrates the usage of Qt Signon APIs.
        It is a GUI application that can be used to view available Mechanisams.
        This will also show the available identities and you can create your own identities as well.

BUILDING
        The application is using Qt build system, so use usual qmake/make:
        $ qmake
        $ make

RUNNING
        You can run the application by typing  ./signon-qt-example

	Steps:
            *. After running the application.
	 There are three tabular items.
            1. In first tab, you will have the required text box entries to create the identity and store button to store the identity.
               and there is a list of methods in the identity in the list widget.
            2. In second tab, you will have two list widgets and two push buttons.One list widget is for existing identities 
               and another for list of methods of perticular identities.Challenge button is for session response and
               remove button to remove the identity.
            3. In third tab, you will have combo box of having the methods list and list widget of having mechanisms list which are supported by the methods.
            4. Exit button is common in all the tablular items to come out of the application.

README (END)

