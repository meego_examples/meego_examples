/* The code examples copyrighted by Nokia Corporation that are included to
 * this material are licensed to you under following MIT-style License:
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <QtGui>
#include <QDebug>
#include <QMessageBox>
#include <QSignalSpy>

#include "SignOn/AuthService"
#include "SignOn/AuthSession"
#include "SignOn/Identity"
#include "signonexample.h"


SignonExample::SignonExample()
{
    setupUi(this);

    authService=new SignOn::AuthService();
    connect(authService, SIGNAL(methodsAvailable(const QStringList&)),
            this, SLOT(methodsAvailable(const QStringList&)));
    connect(authService, SIGNAL(mechanismsAvailable(const QString &, const QStringList&)),
            this, SLOT( mechanismsAvailable(const QString &,  const QStringList&)));
    connect(authService, SIGNAL(identities(const QList<SignOn::IdentityInfo>&)),
            this, SLOT(identities(const QList<SignOn::IdentityInfo>&)));
    connect(authService, SIGNAL(error(const SignOn::Error &)),
            this, SLOT(error(const SignOn::Error &)));

    // Query available methods & identities
    QSignalSpy spyMethods(authService, SIGNAL(methodsAvailable(const QStringList &)));
    authService->queryMethods();

    if(spyMethods.count())
    {
        methodsComboBox->setToolTip("checking available plugins");

    }
    else
    {
        passwordLineEdit->setEnabled(false);
        captionLineEdit->setEnabled(false);
        usernameLineEdit->setEnabled(false);
        storeButton->setEnabled(false);
        challengeButton->setEnabled(false);
        methodsComboBox->setToolTip("No plugins found");
    }
    authService->queryIdentities();
}

SignonExample::~SignonExample()
{
}

// Adds available methods to combo box and list widget for new identities
void SignonExample::methodsAvailable(const QStringList& mechs)
{

    qDebug()<<mechs.count()<<"mechs count is";

    passwordLineEdit->setEnabled(true);
    captionLineEdit->setEnabled(true);
    usernameLineEdit->setEnabled(true);
    storeButton->setEnabled(true);
    challengeButton->setEnabled(true);
    //    methodsComboBox->setToolTip("No Plugins");

    methodsComboBox->clear();
    methodsComboBox->addItems(mechs);

    // Clear selection
    methodsComboBox->setCurrentIndex(-1);
    newIdentityMethodsListWidget->clear();
    newIdentityMethodsListWidget->addItems(mechs);
}

// Updates mechanism list for specified method with specified stringlist
void SignonExample::mechanismsAvailable(const QString &method, const QStringList& mechs)
{
    // method name already visible in combo box, so we don't add it anywhere
    Q_UNUSED(method);
    mechanismsListWidget->clear();
    mechanismsListWidget->addItems(mechs);
}

// Identity was removed, query for new list
void SignonExample::identityRemoved()
{
    identityMethodsListWidget->clear();
    QMessageBox::information(this, QString("identity removed"),"Removed", QMessageBox::Ok);
    authService->queryIdentities();
}

// Got list of identities, replace current list with them
void SignonExample::identities(const QList<SignOn::IdentityInfo>& identityList)
{
    identityListWidget->clear();
    for (int i = 0; i < identityList.size(); ++i) {
        QListWidgetItem *item = new QListWidgetItem(identityList.at(i).caption());
        item->setData(Qt::UserRole, QVariant(identityList.at(i).id()));
        identityListWidget->addItem(item);
    }
}

// Identity selection changed, update methods list accordingly
void SignonExample::on_identityListWidget_itemSelectionChanged()
{
    qDebug("SelectionChanged");
    QListWidgetItem *item = identityListWidget->currentItem();
    if (item != NULL) {
        removeIdentityButton->setEnabled(true);
        qint32 index = item->data(Qt::UserRole).toInt();
        SignOn::Identity *identity;
        identity = SignOn::Identity::existingIdentity(index);
        connect(identity, SIGNAL(methodsAvailable(const QStringList &)),
                this, SLOT(identityMethodsAvailable(const QStringList &)));

        identity->queryAvailableMethods();
    }
}

// We got list of methods, add them to the list
void SignonExample::identityMethodsAvailable(const QStringList &methods)
{
    identityMethodsListWidget->clear();
    identityMethodsListWidget->addItems(methods);
}

void SignonExample::sessionResponse(const SignOn::SessionData &sessionData)
{
    Q_UNUSED(sessionData);
    QStringList list = sessionData.propertyNames();
    QMessageBox::critical(this,"response();",list.at(0),QMessageBox::Ok);
    label->setText(list.at(0));


}

// Error occured, alert user.
void SignonExample::error(const SignOn::Error &err)
{
    QString ee=  err.message();
    QMessageBox::critical(this,"error();",ee,QMessageBox::Ok);

}

// Credentials successfully stored, show message to user
void SignonExample::credentialsStored(const quint32 id)
{
    QString message;
    message.setNum(id);
    QMessageBox::information(this, QString("Credentials stored"), message, QMessageBox::Ok);
    authService->queryIdentities();
}

// Combo box item activated, query available mechanisms for list widget
void SignonExample::on_methodsComboBox_activated(const QString & text)
{
    qDebug() << "activated:" << text;
    authService->queryMechanisms(text);
}

// Remove selected identity when remove button is clicked
void SignonExample::on_removeIdentityButton_clicked()
{
    QListWidgetItem *item = identityListWidget->currentItem();
    if (item != NULL) {
        int index = item->data(Qt::UserRole).toInt();
        SignOn::Identity *identity;
        identity = SignOn::Identity::existingIdentity(index);
        connect(identity, SIGNAL(removed()),
                this, SLOT(identityRemoved()));
        identity->remove();
        removeIdentityButton->setEnabled(false);
    }
}

// Store button clicked, save details
void SignonExample::on_storeButton_clicked()
{
    QString caption = captionLineEdit->text();
    QString username = usernameLineEdit->text();
    // Store only if username and caption are not empty. Password can be empty.
    if (caption != "" && username != "") {
        SignOn::Identity* identity;
        QMap<SignOn::MethodName, SignOn::MechanismsList>  methods;
        QList<QListWidgetItem *> items = newIdentityMethodsListWidget->selectedItems();
        QListWidgetItem *item;
        foreach(item, items) {
            methods.insert(item->text(), QStringList());
        }

        SignOn::IdentityInfo *identityInfo = new SignOn::IdentityInfo(caption, username, methods);
        if (passwordLineEdit->text() != "") {
            identityInfo->setSecret(passwordLineEdit->text());
        }

        identity = SignOn::Identity::newIdentity(*identityInfo);

        connect(identity, SIGNAL(credentialsStored(const quint32)),
                this, SLOT(credentialsStored(const quint32)));

        connect(identity, SIGNAL(error(const Error &)),
                this, SLOT(error(const Error &)));

        identity->storeCredentials(*identityInfo);
    }
}

// Challenge button clicked, if item selected, try challenging selected item
void SignonExample::on_challengeButton_clicked()
{
    QListWidgetItem *item = identityListWidget->currentItem();
    if (item != NULL) {
        qint32 index = item->data(Qt::UserRole).toInt();
        SignOn::Identity *identity = SignOn::Identity::existingIdentity(index);
        //TODO: Connect error
        connect(identity, SIGNAL(info(const SignOn::IdentityInfo&)),
                this, SLOT(userInfoAvailable(const SignOn::IdentityInfo&)));
        identity->queryInfo();
    }
}

// Identity info available, try logging in if using google
void SignonExample::userInfoAvailable(const SignOn::IdentityInfo& info)
{
    SignOn::AuthSession *session;
    SignOn::SessionData data;
    data.setUserName(info.userName());
    SignOn::Identity *identity = SignOn::Identity::newIdentity(info);
    QList<QListWidgetItem *> items = identityMethodsListWidget->selectedItems();
    QListWidgetItem *item;
    foreach(item, items) {
        if (item->text() == "google") {
            session = identity->createSession(QString("google"));
            connect(session, SIGNAL(response(const SessionData&)),
                    this, SLOT(sessionResponse(const SessionData&)));
            connect(session, SIGNAL(error(const Error &)),
                    this, SLOT(error(const Error &)));
            SignOn::SessionData *sessionData = new SignOn::SessionData(data);
            session->process(*sessionData, QString("AuthSub"));
        }
        else if(item->text() == "facebook")
        {
            session = identity->createSession(QString("facebook"));
            connect(session, SIGNAL(response(const SessionData&)),
                    this, SLOT(sessionResponse(const SessionData&)));
            connect(session, SIGNAL(error(const Error &)),
                    this, SLOT(error(const Error &)));
            SignOn::SessionData *sessionData = new SignOn::SessionData(data);
            session->process(*sessionData, QString("AuthLogin"));
        }
        else if(item->text() == "oviauth")
        {
            session = identity->createSession(QString("oviauth"));
            connect(session, SIGNAL(response(const SessionData&)),
                    this, SLOT(sessionResponse(const SessionData&)));
            connect(session, SIGNAL(error(const Error &)),
                    this, SLOT(error(const Error &)));
            SignOn::SessionData *sessionData = new SignOn::SessionData(data);
            session->process(*sessionData, QString("initialise"));
        }
        else if(item->text() == "flickr")
        {
            session = identity->createSession(QString("flickr"));
            connect(session, SIGNAL(response(const SessionData&)),
                    this, SLOT(sessionResponse(const SessionData&)));
            connect(session, SIGNAL(error(const Error &)),
                    this, SLOT(error(const Error &)));
            SignOn::SessionData *sessionData = new SignOn::SessionData(data);
            session->process(*sessionData, QString(""));
        }
        else {
            qDebug() << "Currently unhandled method:" << item->text();
            QMessageBox::critical(this,"error();","myText",QMessageBox::Ok);
        }
    }
}


void SignonExample::on_exitButton_clicked()
{
    this->close();
}


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    SignonExample signonExample ;
    signonExample.showMaximized();
    return app.exec();
}

